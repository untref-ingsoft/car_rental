#!/bin/bash

set -e

function check() {
  ruby app.rb "$2" "$3" "$4" "$5" "$6" | grep "$7" >/dev/null && echo "$1: ok" || echo "$1: error"
}

# ruby app.rb <fecha_alquiler> <fecha_devolucion> <cuit> <tipo_alquiler> <parametros_alquiler>

check '01-alquiler por hora' 20190119 20190119 20112223336 h 3 'Importe: 300'
check '02-alquier por dia' 20190119 20190119 20112223336 d 1 'Importe: 2000'
check '03-alquiler por km' 20190119 20190119 20112223336 k 1 'Importe: 110'
check '04-alquier por dia empresa' 20190119 20190119 26112223336 d 1 'Importe: 1900'
check '05-alquier miercoles' 20190821 20190821 20112223336 h 3 'Importe: 250'
check '06-alquier devuelto tarde' 20190823 20190824 20112223336 h 3 'Importe: 600'
