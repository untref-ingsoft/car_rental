Car Rental
===========

Una empresa de alquiler de autos ofrece distintas modalidades de alquiler:

* Por hora: el cliente debe pagar por cada hora que alquila el auto. El costo es de $ 100 / hora.
* Por día: el cliente paga un monto fijo por día (el día son 24 horas) no importa si el auto se devuelve antes. La cantidad de días debe definir al momento del alquiler. El costo es de $ 2000 / día
* Por kilometraje: el cliente paga un precio fijo por cada kilómetros recorrido durante el período de alquiler. Este tipo de alquiler implica devolución dentro del mismo día de alquiler. El costo $ 100 de base más $ 10/km.

Al mismo tiempo hay una serie de reglas de facturación:

* Los dias miércoles todos los alquileres tienen una bonifcación de $ 50.
* Si quien alquila es una empresa (CUIT empieza con 26) tiene un descuento del 5% como parte de la política de fidelización de clientes
* Si el auto es devuelto luego de finalizado el tiempo establecido al momento de alquiler, se cobra un recargo del 100%.

Se pide hacer una aplicación de línea de comando que permita registrar alguileres y calcular la correspondiente facturación.

El desarrollo de la aplicación debe hacerse sigiuendo el ciclo BDD + TDD.

El archivo acceptance_test.sh contiene un conjunto de pruebas funcionales desde la perspectiva de usuario y debe usarse para guiar el ciclo macro propuesto por BDD.

Las pruebas de índole unitarias correspondiente al ciclo de TDD deben ser diseñadas y construidas como parte de la consigna. Dichas pruebas dependerán del diseño de objetos que cada uno realice.

Se espera que en la implementación de esta aplicación se apliquen los conceptos de diseño orientado a objeto vistos en esta materia y también en materias anteriores. También deben aplicarse los conceptos de Walking Skeleton y Arquitectura Hexagonal.

Tener presente que Gitlab no ejecutará las pruebas de aceptación con lo cual pueden (y deben) hacer commit+push aún cuando tengan pruebas de aceptación que no estén funcionando. Sin embargo a momento de la entrega debe asegurarse que la aplicación pase la totalidad de las pruebas de aceptación, de lo contrario el ejercicio estará automáticamente desaprobado.

Importante: no basta con que la aplicación pase las pruebas de aceptación, eso es solo condición necesaria pero no suficiente. Es necesario que la aplicación tenga la lógica completa de cálculo de envido.

Al finalizar la tarea, deben entregar el link al correspondiente merge-request en sus repositorios personales.
